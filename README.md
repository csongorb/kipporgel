# kipporgel

A short (work in progress) experiment by [**csongor baranyai**](http://www.csongorb.com).

Made with:

- [Processing](http://processing.org) 3.5.4
- [Processing Android](http://android.processing.org) 4.2.1
- [Processing Sound 2.3.1](https://github.com/processing/processing-sound/releases)
- either/or
	- some sound samples I'm still trying to find (you can find all the samples I'm testing in the data-folder)

Read the [journal](/JOURNAL.md) for further insights into the development process.

## How to use?

- Accelerometer: increase / decrease speed
- Touch: create / delete note
- Swipe: switch between songs

## License

[![Creative Commons License](https://i.creativecommons.org/l/by-nc/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc/4.0/)
This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License](http://creativecommons.org/licenses/by-nc/4.0/).

## Changelog

### Ideas / Bugs

- pinch to change the length of a song
- using the back button to go to the main menu again (?)
    - https://android.processing.org/reference/events/backpress.html
- change song-length to something time-unrelated, as this is a non-sequitur
- credits
- title (?)
- app icon
- (some kind of) options
    - adjust zero-alignment of the accelerometer
- bug / issue
    - screen orientation thing
	    - project rotates between portrait and landscape
		- https://developer.android.com/guide/topics/resources/runtime-changes#java

### 0.3 - Work in Progress (Mid-2019 & End-2021)

Improvements:

- preparing for touch
	- centered click from mouse position
- improved visuals
	- supporting sound anticipation
- architecture
	- encapsulation of the drawing function
	- vertical drawing possible!
- app (sound playing) pauses when paused in Android
- app doesn't go to sleep when playing
- added some new sounds (testing)
- added vibration
- added swipe to change between songs

Changes:

- android! accelerometer!
    - changed to landscape mode
- preparing for android
    - changed from Minim to Processing Sound
	- playing audio-files instead of notes
- notes/strings as objects
	- preparing for additional notes/strings

Bugs:
- sounds are glitching when played too fast
	- https://github.com/processing/processing-sound/issues/17
- swipe (between songs) bugs
	- on the two sides (first & last song) the songs are displayed twice (and other strange behaviour)

### 0.2 - Songs & Notes (8. August 2015)

- added songs
- more precise notes
    - changed from frequences to notes (CDEFGABC)
- supporting anticipation (through visuals)
    - emphasising the moment prior to a note!

### 0.1 - First (6. August 2015)

- first version
- desktop
