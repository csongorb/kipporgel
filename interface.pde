void touchStarted() {
  isTouch = true;

  touchStartPos.x = touches[0].x;
  touchStartPos.y = touches[0].y;

  //println("touchStarted (" + touchStartPos + ")");
}

void touchMoved() {

  touchVector.x = touches[0].x - touchStartPos.x;
  touchVector.y = touches[0].y - touchStartPos.y;

  //println("touchMoved (" + touchVector.mag() + ")");

  isPlaying = false;

  if (touchVector.mag() >= 100) {
    isTouch = false;
    isLRswipe = true;
  }
}

void touchEnded() {
  println("touchEnded");

  isPlaying = true;

  // =========================
  // when a swipe ends

  if (!isTouch) {
    //println("swipe direction: " + touchVector.heading());

    int swipeTiltingPoint = width / 3;

    float vDir = 0.5;
    if (touchVector.heading() < vDir && touchVector.heading() > -vDir) {
      //println ("swipe right");

      if (touchVector.x > swipeTiltingPoint) {
        songPlaying--;
        if (songPlaying <= 0) {
          songPlaying = 0;
        }
        isSnapForward = true;
      } else {
        isSnapBack = true;
      }
    }
    if ((touchVector.heading() < PI && touchVector.heading() > (PI - vDir)) || (touchVector.heading() > -PI && touchVector.heading() < (-PI + vDir))) {
      //println ("swipe left");

      if (touchVector.x < -swipeTiltingPoint) {
        songPlaying++;
        if (songPlaying >= mySongs.length) {
          songPlaying = mySongs.length - 1;
        }
        isSnapForward = true;
      } else {
        isSnapBack = true;
      }
    }

    if (isSnapBack) {
      println("will have to snap back...");
    }
    if (isSnapForward) {
      println("will have to snap forward...");
    } 

    isLRswipe = false;
  }

  // =========================
  // when a touch ends

  if (isTouch) {

    if (mouseX >= drawXPos && mouseX < drawXPos + drawWidth && mouseY >= drawYPos && mouseY < drawYPos + drawHeight) { 

      boolean noteThere = false;
      int existingNoteNr = 0;

      //is there a note already?
      for (int i = 0; i < mySongs[songPlaying].sounds.size(); i++) {
        Sound sound = mySongs[songPlaying].sounds.get(i);
        if (sound.pos == mouseOverLoopStep()) {
          if (sound.note == myInstStrings[mouseOverNoteNr()].note) {
            noteThere = true;
            existingNoteNr = i;
          }
        }
      } 

      if (noteThere) {
        mySongs[songPlaying].sounds.remove(existingNoteNr);
      } else {
        mySongs[songPlaying].sounds.add(new Sound(mouseOverLoopStep(), myInstStrings[mouseOverNoteNr()].note));
        playString(myInstStrings[mouseOverNoteNr()].note);
      }
    }
  }
}
