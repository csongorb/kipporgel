Sound pack downloaded from Freesound
----------------------------------------

This pack of sounds contains sounds by the following user:
 - poissonmort ( https://freesound.org/people/poissonmort/ )

You can find this pack online at: https://freesound.org/people/poissonmort/packs/15524/

License details
---------------

Attribution: http://creativecommons.org/licenses/by/3.0/


Sounds in this pack
-------------------

  * 253276__poissonmort__toy-records-17-b3-03.wav
    * url: https://freesound.org/s/253276/
    * license: Attribution
  * 253275__poissonmort__toy-records-18-c4-01.wav
    * url: https://freesound.org/s/253275/
    * license: Attribution
  * 253274__poissonmort__toy-records-17-b3-01.wav
    * url: https://freesound.org/s/253274/
    * license: Attribution
  * 253273__poissonmort__toy-records-17-b3-02.wav
    * url: https://freesound.org/s/253273/
    * license: Attribution
  * 253272__poissonmort__toy-records-15-a-3-02.wav
    * url: https://freesound.org/s/253272/
    * license: Attribution
  * 253271__poissonmort__toy-records-15-a-3-03.wav
    * url: https://freesound.org/s/253271/
    * license: Attribution
  * 253270__poissonmort__toy-records-13-a3-03.wav
    * url: https://freesound.org/s/253270/
    * license: Attribution
  * 253269__poissonmort__toy-records-15-a-3-01.wav
    * url: https://freesound.org/s/253269/
    * license: Attribution
  * 253268__poissonmort__toy-records-23-f4-04.wav
    * url: https://freesound.org/s/253268/
    * license: Attribution
  * 253267__poissonmort__toy-records-99-k01.wav
    * url: https://freesound.org/s/253267/
    * license: Attribution
  * 253266__poissonmort__toy-records-22-e4-01.wav
    * url: https://freesound.org/s/253266/
    * license: Attribution
  * 253265__poissonmort__toy-records-99-k13.wav
    * url: https://freesound.org/s/253265/
    * license: Attribution
  * 253264__poissonmort__toy-records-99-k10.wav
    * url: https://freesound.org/s/253264/
    * license: Attribution
  * 253263__poissonmort__toy-records-18-c4-02.wav
    * url: https://freesound.org/s/253263/
    * license: Attribution
  * 253262__poissonmort__toy-records-18-c4-03.wav
    * url: https://freesound.org/s/253262/
    * license: Attribution
  * 253261__poissonmort__toy-records-22-e4-02.wav
    * url: https://freesound.org/s/253261/
    * license: Attribution
  * 253260__poissonmort__toy-records-99-k04.wav
    * url: https://freesound.org/s/253260/
    * license: Attribution
  * 253259__poissonmort__toy-records-09-f-3-01.wav
    * url: https://freesound.org/s/253259/
    * license: Attribution
  * 253258__poissonmort__toy-records-09-f-3-02.wav
    * url: https://freesound.org/s/253258/
    * license: Attribution
  * 253257__poissonmort__toy-records-22-e4-03.wav
    * url: https://freesound.org/s/253257/
    * license: Attribution
  * 253256__poissonmort__toy-records-06-e3-03.wav
    * url: https://freesound.org/s/253256/
    * license: Attribution
  * 253255__poissonmort__toy-records-07-f3-01.wav
    * url: https://freesound.org/s/253255/
    * license: Attribution
  * 253254__poissonmort__toy-records-07-f3-02.wav
    * url: https://freesound.org/s/253254/
    * license: Attribution
  * 253253__poissonmort__toy-records-07-f3-03.wav
    * url: https://freesound.org/s/253253/
    * license: Attribution
  * 253252__poissonmort__toy-records-04-d-3-02.wav
    * url: https://freesound.org/s/253252/
    * license: Attribution
  * 253251__poissonmort__toy-records-04-d-3-03.wav
    * url: https://freesound.org/s/253251/
    * license: Attribution
  * 253250__poissonmort__toy-records-06-e3-01.wav
    * url: https://freesound.org/s/253250/
    * license: Attribution
  * 253249__poissonmort__toy-records-06-e3-02.wav
    * url: https://freesound.org/s/253249/
    * license: Attribution
  * 253248__poissonmort__toy-records-23-f4-01.wav
    * url: https://freesound.org/s/253248/
    * license: Attribution
  * 253247__poissonmort__toy-records-21-d-4-03.wav
    * url: https://freesound.org/s/253247/
    * license: Attribution
  * 253246__poissonmort__toy-records-21-d-4-02.wav
    * url: https://freesound.org/s/253246/
    * license: Attribution
  * 253245__poissonmort__toy-records-22-e4-04.wav
    * url: https://freesound.org/s/253245/
    * license: Attribution
  * 253244__poissonmort__toy-records-20-d4-01.wav
    * url: https://freesound.org/s/253244/
    * license: Attribution
  * 253243__poissonmort__toy-records-99-k05.wav
    * url: https://freesound.org/s/253243/
    * license: Attribution
  * 253242__poissonmort__toy-records-99-k03.wav
    * url: https://freesound.org/s/253242/
    * license: Attribution
  * 253241__poissonmort__toy-records-21-d-4-04.wav
    * url: https://freesound.org/s/253241/
    * license: Attribution
  * 253240__poissonmort__toy-records-99-k02.wav
    * url: https://freesound.org/s/253240/
    * license: Attribution
  * 253239__poissonmort__toy-records-23-f4-03.wav
    * url: https://freesound.org/s/253239/
    * license: Attribution
  * 253238__poissonmort__toy-records-99-k06.wav
    * url: https://freesound.org/s/253238/
    * license: Attribution
  * 253237__poissonmort__toy-records-12-g-3-01.wav
    * url: https://freesound.org/s/253237/
    * license: Attribution
  * 253236__poissonmort__toy-records-10-g3-04.wav
    * url: https://freesound.org/s/253236/
    * license: Attribution
  * 253235__poissonmort__toy-records-12-g-3-03.wav
    * url: https://freesound.org/s/253235/
    * license: Attribution
  * 253234__poissonmort__toy-records-12-g-3-02.wav
    * url: https://freesound.org/s/253234/
    * license: Attribution
  * 253233__poissonmort__toy-records-10-g3-01.wav
    * url: https://freesound.org/s/253233/
    * license: Attribution
  * 253232__poissonmort__toy-records-09-f-3-03.wav
    * url: https://freesound.org/s/253232/
    * license: Attribution
  * 253231__poissonmort__toy-records-10-g3-03.wav
    * url: https://freesound.org/s/253231/
    * license: Attribution
  * 253230__poissonmort__toy-records-10-g3-02.wav
    * url: https://freesound.org/s/253230/
    * license: Attribution
  * 253229__poissonmort__toy-records-21-d-4-01.wav
    * url: https://freesound.org/s/253229/
    * license: Attribution
  * 253228__poissonmort__toy-records-20-d4-04.wav
    * url: https://freesound.org/s/253228/
    * license: Attribution
  * 253227__poissonmort__toy-records-20-d4-03.wav
    * url: https://freesound.org/s/253227/
    * license: Attribution
  * 253226__poissonmort__toy-records-20-d4-02.wav
    * url: https://freesound.org/s/253226/
    * license: Attribution
  * 253225__poissonmort__toy-records-13-a3-02.wav
    * url: https://freesound.org/s/253225/
    * license: Attribution
  * 253224__poissonmort__toy-records-13-a3-01.wav
    * url: https://freesound.org/s/253224/
    * license: Attribution
  * 253223__poissonmort__toy-records-19-c-4-02.wav
    * url: https://freesound.org/s/253223/
    * license: Attribution
  * 253222__poissonmort__toy-records-19-c-4-01.wav
    * url: https://freesound.org/s/253222/
    * license: Attribution
  * 253221__poissonmort__toy-records-19-c-4-03.wav
    * url: https://freesound.org/s/253221/
    * license: Attribution
  * 253220__poissonmort__toy-records-99-k07.wav
    * url: https://freesound.org/s/253220/
    * license: Attribution
  * 253219__poissonmort__toy-records-99-k09.wav
    * url: https://freesound.org/s/253219/
    * license: Attribution
  * 253218__poissonmort__toy-records-99-k08.wav
    * url: https://freesound.org/s/253218/
    * license: Attribution
  * 253217__poissonmort__toy-records-99-k12.wav
    * url: https://freesound.org/s/253217/
    * license: Attribution
  * 253216__poissonmort__toy-records-23-f4-02.wav
    * url: https://freesound.org/s/253216/
    * license: Attribution
  * 253215__poissonmort__toy-records-99-k11.wav
    * url: https://freesound.org/s/253215/
    * license: Attribution
  * 253214__poissonmort__toy-records-03-d3-03.wav
    * url: https://freesound.org/s/253214/
    * license: Attribution
  * 253213__poissonmort__toy-records-04-d-3-01.wav
    * url: https://freesound.org/s/253213/
    * license: Attribution
  * 253212__poissonmort__toy-records-02-c-3-02.wav
    * url: https://freesound.org/s/253212/
    * license: Attribution
  * 253211__poissonmort__toy-records-02-c-3-03.wav
    * url: https://freesound.org/s/253211/
    * license: Attribution
  * 253210__poissonmort__toy-records-03-d3-01.wav
    * url: https://freesound.org/s/253210/
    * license: Attribution
  * 253209__poissonmort__toy-records-03-d3-02.wav
    * url: https://freesound.org/s/253209/
    * license: Attribution
  * 253208__poissonmort__toy-records-01-c3-01.wav
    * url: https://freesound.org/s/253208/
    * license: Attribution
  * 253207__poissonmort__toy-records-01-c3-02.wav
    * url: https://freesound.org/s/253207/
    * license: Attribution
  * 253206__poissonmort__toy-records-01-c3-03.wav
    * url: https://freesound.org/s/253206/
    * license: Attribution
  * 253205__poissonmort__toy-records-02-c-3-01.wav
    * url: https://freesound.org/s/253205/
    * license: Attribution


