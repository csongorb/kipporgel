Sound pack downloaded from Freesound
----------------------------------------

This pack of sounds contains sounds by the following user:
 - modularsamples ( https://freesound.org/people/modularsamples/ )

You can find this pack online at: https://freesound.org/people/modularsamples/packs/17677/

License details
---------------

Creative Commons 0: http://creativecommons.org/publicdomain/zero/1.0/


Sounds in this pack
-------------------

  * 314965__modularsamples__yamaha-cs-30l-toy-piano-c5-small-plick-72-127.aiff
    * url: https://freesound.org/s/314965/
    * license: Creative Commons 0
  * 314964__modularsamples__yamaha-cs-30l-toy-piano-b4-small-plick-71-127.aiff
    * url: https://freesound.org/s/314964/
    * license: Creative Commons 0
  * 314963__modularsamples__yamaha-cs-30l-toy-piano-a-4-small-plick-70-127.aiff
    * url: https://freesound.org/s/314963/
    * license: Creative Commons 0
  * 314962__modularsamples__yamaha-cs-30l-toy-piano-a4-small-plick-69-127.aiff
    * url: https://freesound.org/s/314962/
    * license: Creative Commons 0
  * 314961__modularsamples__yamaha-cs-30l-toy-piano-g4-small-plick-68-127.aiff
    * url: https://freesound.org/s/314961/
    * license: Creative Commons 0
  * 314960__modularsamples__yamaha-cs-30l-toy-piano-f-4-small-plick-67-127.aiff
    * url: https://freesound.org/s/314960/
    * license: Creative Commons 0
  * 314959__modularsamples__yamaha-cs-30l-toy-piano-f4-small-plick-66-127.aiff
    * url: https://freesound.org/s/314959/
    * license: Creative Commons 0
  * 314958__modularsamples__yamaha-cs-30l-toy-piano-e-4-small-plick-65-127.aiff
    * url: https://freesound.org/s/314958/
    * license: Creative Commons 0
  * 314957__modularsamples__yamaha-cs-30l-toy-piano-e4-small-plick-64-127.aiff
    * url: https://freesound.org/s/314957/
    * license: Creative Commons 0
  * 314956__modularsamples__yamaha-cs-30l-toy-piano-d-4-small-plick-63-127.aiff
    * url: https://freesound.org/s/314956/
    * license: Creative Commons 0
  * 314955__modularsamples__yamaha-cs-30l-toy-piano-d4-small-plick-62-127.aiff
    * url: https://freesound.org/s/314955/
    * license: Creative Commons 0
  * 314954__modularsamples__yamaha-cs-30l-toy-piano-c-4-small-plick-61-127.aiff
    * url: https://freesound.org/s/314954/
    * license: Creative Commons 0
  * 314953__modularsamples__yamaha-cs-30l-toy-piano-c4-small-plick-60-127.aiff
    * url: https://freesound.org/s/314953/
    * license: Creative Commons 0
  * 314952__modularsamples__yamaha-cs-30l-toy-piano-b3-small-plick-59-127.aiff
    * url: https://freesound.org/s/314952/
    * license: Creative Commons 0
  * 314951__modularsamples__yamaha-cs-30l-toy-piano-a-3-small-plick-58-127.aiff
    * url: https://freesound.org/s/314951/
    * license: Creative Commons 0
  * 314950__modularsamples__yamaha-cs-30l-toy-piano-a3-small-plick-57-127.aiff
    * url: https://freesound.org/s/314950/
    * license: Creative Commons 0
  * 314949__modularsamples__yamaha-cs-30l-toy-piano-g3-small-plick-56-127.aiff
    * url: https://freesound.org/s/314949/
    * license: Creative Commons 0
  * 314948__modularsamples__yamaha-cs-30l-toy-piano-f-3-small-plick-55-127.aiff
    * url: https://freesound.org/s/314948/
    * license: Creative Commons 0
  * 314947__modularsamples__yamaha-cs-30l-toy-piano-f3-small-plick-54-127.aiff
    * url: https://freesound.org/s/314947/
    * license: Creative Commons 0
  * 314946__modularsamples__yamaha-cs-30l-toy-piano-e-3-small-plick-53-127.aiff
    * url: https://freesound.org/s/314946/
    * license: Creative Commons 0
  * 314945__modularsamples__yamaha-cs-30l-toy-piano-e3-small-plick-52-127.aiff
    * url: https://freesound.org/s/314945/
    * license: Creative Commons 0
  * 314944__modularsamples__yamaha-cs-30l-toy-piano-d-3-small-plick-51-127.aiff
    * url: https://freesound.org/s/314944/
    * license: Creative Commons 0
  * 314943__modularsamples__yamaha-cs-30l-toy-piano-d3-small-plick-50-127.aiff
    * url: https://freesound.org/s/314943/
    * license: Creative Commons 0
  * 314942__modularsamples__yamaha-cs-30l-toy-piano-c-3-small-plick-49-127.aiff
    * url: https://freesound.org/s/314942/
    * license: Creative Commons 0
  * 314941__modularsamples__yamaha-cs-30l-toy-piano-c3-small-plick-48-127.aiff
    * url: https://freesound.org/s/314941/
    * license: Creative Commons 0
  * 314940__modularsamples__yamaha-cs-30l-toy-piano-b2-small-plick-47-127.aiff
    * url: https://freesound.org/s/314940/
    * license: Creative Commons 0
  * 314939__modularsamples__yamaha-cs-30l-toy-piano-a-2-small-plick-46-127.aiff
    * url: https://freesound.org/s/314939/
    * license: Creative Commons 0
  * 314938__modularsamples__yamaha-cs-30l-toy-piano-a2-small-plick-45-127.aiff
    * url: https://freesound.org/s/314938/
    * license: Creative Commons 0
  * 314937__modularsamples__yamaha-cs-30l-toy-piano-g2-small-plick-44-127.aiff
    * url: https://freesound.org/s/314937/
    * license: Creative Commons 0
  * 314936__modularsamples__yamaha-cs-30l-toy-piano-f-2-small-plick-43-127.aiff
    * url: https://freesound.org/s/314936/
    * license: Creative Commons 0
  * 314935__modularsamples__yamaha-cs-30l-toy-piano-f2-small-plick-42-127.aiff
    * url: https://freesound.org/s/314935/
    * license: Creative Commons 0
  * 314934__modularsamples__yamaha-cs-30l-toy-piano-e-2-small-plick-41-127.aiff
    * url: https://freesound.org/s/314934/
    * license: Creative Commons 0
  * 314933__modularsamples__yamaha-cs-30l-toy-piano-e2-small-plick-40-127.aiff
    * url: https://freesound.org/s/314933/
    * license: Creative Commons 0
  * 314932__modularsamples__yamaha-cs-30l-toy-piano-d-2-small-plick-39-127.aiff
    * url: https://freesound.org/s/314932/
    * license: Creative Commons 0
  * 314931__modularsamples__yamaha-cs-30l-toy-piano-d2-small-plick-38-127.aiff
    * url: https://freesound.org/s/314931/
    * license: Creative Commons 0
  * 314930__modularsamples__yamaha-cs-30l-toy-piano-c-2-small-plick-37-127.aiff
    * url: https://freesound.org/s/314930/
    * license: Creative Commons 0
  * 314929__modularsamples__yamaha-cs-30l-toy-piano-c2-small-plick-36-127.aiff
    * url: https://freesound.org/s/314929/
    * license: Creative Commons 0


