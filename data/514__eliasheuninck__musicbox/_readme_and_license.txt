Sound pack downloaded from Freesound
----------------------------------------

This pack of sounds contains sounds by the following user:
 - eliasheuninck ( https://freesound.org/people/eliasheuninck/ )

You can find this pack online at: https://freesound.org/people/eliasheuninck/packs/514/

License details
---------------

Creative Commons 0: http://creativecommons.org/publicdomain/zero/1.0/


Sounds in this pack
-------------------

  * 9287__eliasheuninck__sol_sol_2.aiff
    * url: https://freesound.org/s/9287/
    * license: Creative Commons 0
  * 9286__eliasheuninck__sol_sol_1.aiff
    * url: https://freesound.org/s/9286/
    * license: Creative Commons 0
  * 9285__eliasheuninck__sol_ci.aiff
    * url: https://freesound.org/s/9285/
    * license: Creative Commons 0
  * 9284__eliasheuninck__sol_4.aiff
    * url: https://freesound.org/s/9284/
    * license: Creative Commons 0
  * 9283__eliasheuninck__sol_3.aiff
    * url: https://freesound.org/s/9283/
    * license: Creative Commons 0
  * 9282__eliasheuninck__sol_1.aiff
    * url: https://freesound.org/s/9282/
    * license: Creative Commons 0
  * 9281__eliasheuninck__re_re.aiff
    * url: https://freesound.org/s/9281/
    * license: Creative Commons 0
  * 9280__eliasheuninck__re_4.aiff
    * url: https://freesound.org/s/9280/
    * license: Creative Commons 0
  * 9279__eliasheuninck__re_2.aiff
    * url: https://freesound.org/s/9279/
    * license: Creative Commons 0
  * 9278__eliasheuninck__re_1.aiff
    * url: https://freesound.org/s/9278/
    * license: Creative Commons 0
  * 9277__eliasheuninck__mi_re.aiff
    * url: https://freesound.org/s/9277/
    * license: Creative Commons 0
  * 9276__eliasheuninck__la_2.aiff
    * url: https://freesound.org/s/9276/
    * license: Creative Commons 0
  * 9275__eliasheuninck__la_1.aiff
    * url: https://freesound.org/s/9275/
    * license: Creative Commons 0
  * 9274__eliasheuninck__fa_kruis_re_1.aiff
    * url: https://freesound.org/s/9274/
    * license: Creative Commons 0
  * 9273__eliasheuninck__fa_kruis_4.aiff
    * url: https://freesound.org/s/9273/
    * license: Creative Commons 0
  * 9272__eliasheuninck__fa_kruis_1.aiff
    * url: https://freesound.org/s/9272/
    * license: Creative Commons 0
  * 9271__eliasheuninck__do_kruis_1.aiff
    * url: https://freesound.org/s/9271/
    * license: Creative Commons 0
  * 9270__eliasheuninck__do_chord.aiff
    * url: https://freesound.org/s/9270/
    * license: Creative Commons 0
  * 9269__eliasheuninck__do_1.aiff
    * url: https://freesound.org/s/9269/
    * license: Creative Commons 0
  * 9268__eliasheuninck__ci_fa_kruis.aiff
    * url: https://freesound.org/s/9268/
    * license: Creative Commons 0
  * 9267__eliasheuninck__ci_5.aiff
    * url: https://freesound.org/s/9267/
    * license: Creative Commons 0
  * 9266__eliasheuninck__ci_3.aiff
    * url: https://freesound.org/s/9266/
    * license: Creative Commons 0
  * 9265__eliasheuninck__ci_2.aiff
    * url: https://freesound.org/s/9265/
    * license: Creative Commons 0


