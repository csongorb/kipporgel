class Sound {

  int pos;
  String note;

  boolean play = false;

  int lastPlayedInLoop;
  int xPos, yPos;

  int noteNr = 0;

  Sound(int iPos, String iNote) {
    pos = iPos;
    note = iNote;
    if (pos < loopPos) {
      lastPlayedInLoop = actLoop;
    }

    for (int i = 0; i < nrStrings; i++) {
      if (myInstStrings[i].note == note) {
        noteNr = i;
      }
    }
  }

  void drawInactiveNote(int bgWidth, int bgHeight, float myNoteWidthInPix, float myNoteHeightInPix, color myNoteColor) {

    if (orientationIsHorizontal) {
      xPos = loopPos_To_Px(pos, drawWidth);
      yPos = int((bgHeight - (myNoteHeightInPix * noteNr)) - myNoteHeightInPix);
    } else {
      xPos = int(myNoteWidthInPix * (noteNr + 1));
      yPos = loopPos_To_Px(pos, drawHeight);
    }

    int r = int(myNoteWidthInPix / 5);

    noStroke();
    fill(myNoteColor);
    ellipse(xPos, yPos, r, r);
  }

  void drawActiveNote(int bgWidth, int bgHeight, float myNoteWidthInPix, float myNoteHeightInPix, color myNoteColor, color myLineColor, int mySongLength) {

    // is loopPos (thus the player-head) in the near of a note?
    int sTime = 3000;
    int sDiff = pos - loopPos;
    if ((loopPos > mySongLength - sTime) && (pos < sTime)) {
      sDiff = pos - (loopPos - mySongLength);
    }

    // if yes...
    if ((sDiff > 0) && (sDiff < sTime)) {

      if (orientationIsHorizontal) {
        xPos = loopPos_To_Px(pos, drawWidth);
        yPos = int((bgHeight - (myNoteHeightInPix * noteNr)) - myNoteHeightInPix);
      } else {
        xPos = int(myNoteWidthInPix * (noteNr + 1));
        yPos = loopPos_To_Px(pos, drawHeight);
      }

      int s = int(myNoteWidthInPix * 1.5);

      float f = 1 - (float(sDiff) / float(sTime));
      f = asin(f) / PI * 2;
      //println(f);

      float lerpRed = lerp(red(myLineColor), red(myNoteColor), f);
      float lerpGreen = lerp(green(myLineColor), green(myNoteColor), f);
      float lerpBlue = lerp(blue(myLineColor), blue(myNoteColor), f);
      color c = color(lerpRed, lerpGreen, lerpBlue);

      stroke(c);
      fill(c);

      // draw the dot
      float rToDraw = f * s;
      ellipse(xPos, yPos, rToDraw, rToDraw);

      // draw the lines
      strokeWeight(int(myNoteWidthInPix / 15) * f);
      if (loopPos > (mySongLength - sTime)) {
        if (pos > (mySongLength - sTime)) {
          if (orientationIsHorizontal) {
            line(loopPos_To_Px(loopPos, drawWidth) + 1, yPos, xPos, yPos);
          } else {
            line(xPos, loopPos_To_Px(loopPos, drawHeight) + 1, xPos, yPos);
          }
        }
        if (pos < sTime) {
          if (orientationIsHorizontal) {
            line(loopPos_To_Px(loopPos - mySongLength, drawWidth) + 1, yPos, xPos, yPos);
          } else {
            line(xPos, loopPos_To_Px(loopPos - mySongLength, drawHeight) + 1, xPos, yPos);
          }
        }
      } else {
        if (orientationIsHorizontal) {
          line(loopPos_To_Px(loopPos, drawWidth) + 1, yPos, xPos, yPos);
        } else {
          line(xPos, loopPos_To_Px(loopPos, drawHeight) + 1, xPos, yPos);
        }
      }
    }
  }

  void update() {
    if (actLoop > lastPlayedInLoop) {
      if (loopPos > pos) {
        play = true;
        lastPlayedInLoop = actLoop;
      }
    }
  }

  void playNote() {
    if (play == true) {
      if (v.hasVibrator()) {
        v.vibrate(25);
      }
      playString(note);
      //println(note);
      play = false;
    }
  }
}
