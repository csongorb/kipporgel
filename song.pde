class Song {

  int songLength;
  int oneStep = 1000;
  int halfStep = oneStep / 2;
  int firstOct;

  float noteHeightInPix, noteWidthInPix;

  ArrayList<Sound> sounds;

  color bgC;
  color lineC;
  color noteC;
  color timeC;
  color selectC;

  Song () {
  }

  void update() {
    for (int i = 0; i < sounds.size (); i++) {
      Sound sound = sounds.get(i);
      sound.update();
    }
  }

  void play() {
    for (int i = 0; i < sounds.size (); i++) {
      Sound sound = sounds.get(i);
      sound.playNote();
    }
  }

  void drawEverything(int xPos, int yPos, int bgWidth, int bgHeight) {
    drawBackground(xPos, yPos, bgWidth, bgHeight);
    drawActiveNotes(xPos, yPos, bgWidth, bgHeight);
    drawTime(xPos, yPos, bgWidth, bgHeight);
    drawInactiveNotes(xPos, yPos, bgWidth, bgHeight);
  }

  void drawBackground(int xPos, int yPos, int bgWidth, int bgHeight) {

    pushMatrix();
    translate(xPos, yPos);

    noStroke();
    fill(bgC);
    rect(0, 0, bgWidth, bgHeight);

    stroke(lineC);
    strokeWeight(1 * displayDensity);

    // draw rhythm
    for (int i = 0; i < (songLength / oneStep); i++) {
      if (orientationIsHorizontal) {
        float stepWidthInPx = float(bgWidth) / float(songLength / oneStep);
        float lineX = (stepWidthInPx / 4) + (i * stepWidthInPx);
        line (lineX, 0, lineX, bgHeight);
      } else {
        float stepHeightInPx = float(bgHeight) / float(songLength / oneStep);
        float lineY = (stepHeightInPx / 4) + (i * stepHeightInPx);
        line (0, lineY, bgWidth, lineY);
      }
    }

    // draw note-lines
    if (orientationIsHorizontal) {
      noteHeightInPix = float(bgHeight) / float(nrStrings + 1);
      for (int i = 1; i <= nrStrings; i++) {
        int lineYPos = int(i * noteHeightInPix);
        line (0, lineYPos, bgWidth, lineYPos);
      }
    } else {
      noteWidthInPix = float(bgWidth) / float(nrStrings + 1);
      for (int i = 1; i <= nrStrings; i++) {
        int lineXPos = int(i * noteWidthInPix);
        line (lineXPos, 0, lineXPos, bgHeight);
      }
    }

    popMatrix();
  }

  void drawActiveNotes(int bgXPos, int bgYPos, int bgWidth, int bgHeight) {

    pushMatrix();
    translate(bgXPos, bgYPos);

    for (int i = songLength - halfStep; i >= 0; i = i - halfStep) {
      for (int i2 = 0; i2 < sounds.size(); i2++) {
        Sound sound = sounds.get(i2);
        if (i == sound.pos) {
          sound.drawActiveNote(bgWidth, bgHeight, noteWidthInPix, noteHeightInPix, noteC, lineC, songLength);
        }
      }
    }

    popMatrix();
  }

  void drawTime(int drawXPos, int drawYPos, int drawWidth, int drawHeight) {
    pushMatrix();
    translate(drawXPos, drawYPos);

    stroke(timeC);

    if (stringPlucked && (millis() - stringPluckedAt) <= 100) {
      strokeWeight(5 * displayDensity);
      //stringPlucked = false;
    } else {
      strokeWeight(2 * displayDensity);
    }

    if (orientationIsHorizontal) {
      int tXPos = loopPos_To_Px(loopPos, drawWidth);
      line (tXPos, 0, tXPos, drawHeight);
    } else {
      int tYPos = loopPos_To_Px(loopPos, drawHeight);
      line (0, tYPos, drawWidth, tYPos);
    }

    popMatrix();
  }

  void drawInactiveNotes(int bgXPos, int bgYPos, int bgWidth, int bgHeight) {

    pushMatrix();
    translate(bgXPos, bgYPos);

    for (int i = songLength - halfStep; i >= 0; i = i - halfStep) {
      for (int i2 = 0; i2 < sounds.size(); i2++) {
        Sound sound = sounds.get(i2);
        if (i == sound.pos) {
          sound.drawInactiveNote(bgWidth, bgHeight, noteWidthInPix, noteHeightInPix, noteC);
        }
      }
    }

    popMatrix();
  }

  void loadBociBoci() {

    songLength = 16000; //in milliseconds
    firstOct = 3;

    bgC = color (15, 15, 15);
    lineC = color (0, 60, 0);
    noteC = color (0, 255, 0);
    timeC = color (255, 0, 0);
    selectC = color (255, 255, 255);

    sounds = new ArrayList<Sound>();

    sounds.add(new Sound(0, "C3"));
    sounds.add(new Sound(500, "E3"));
    sounds.add(new Sound(1000, "C3"));
    sounds.add(new Sound(1500, "E3"));
    sounds.add(new Sound(2000, "G3"));
    sounds.add(new Sound(3000, "G3"));

    sounds.add(new Sound(4000, "C3"));
    sounds.add(new Sound(4500, "E3"));
    sounds.add(new Sound(5000, "C3"));
    sounds.add(new Sound(5500, "E3"));
    sounds.add(new Sound(6000, "G3"));
    sounds.add(new Sound(7000, "G3"));

    sounds.add(new Sound(8000, "C4"));
    sounds.add(new Sound(8500, "B3"));
    sounds.add(new Sound(9000, "A3"));
    sounds.add(new Sound(9500, "G3"));
    sounds.add(new Sound(10000, "F3"));
    sounds.add(new Sound(11000, "A3"));

    sounds.add(new Sound(12000, "G3"));
    sounds.add(new Sound(12500, "F3"));
    sounds.add(new Sound(13000, "E3"));
    sounds.add(new Sound(13500, "D3"));
    sounds.add(new Sound(14000, "C3"));
    sounds.add(new Sound(15000, "C3"));
  }

  void loadAlleMeineEntchen() {

    songLength = 20000; //in milliseconds
    firstOct = 3;

    bgC = color (20, 0, 0);
    lineC = color (50, 50, 50);
    noteC = color (0, 0, 255);
    timeC = color (0, 255, 0);
    selectC = color (255, 255, 255);

    sounds = new ArrayList<Sound>();

    sounds.add(new Sound(0, "C3"));
    sounds.add(new Sound(500, "D3"));
    sounds.add(new Sound(1000, "E3"));
    sounds.add(new Sound(1500, "F3"));
    sounds.add(new Sound(2000, "G3"));
    sounds.add(new Sound(3000, "G3"));

    sounds.add(new Sound(4000, "A3"));
    sounds.add(new Sound(4500, "A3"));
    sounds.add(new Sound(5000, "A3"));
    sounds.add(new Sound(5500, "A3"));
    sounds.add(new Sound(6000, "G3"));

    sounds.add(new Sound(8000, "A3"));
    sounds.add(new Sound(8500, "A3"));
    sounds.add(new Sound(9000, "A3"));
    sounds.add(new Sound(9500, "A3"));
    sounds.add(new Sound(10000, "G3"));

    sounds.add(new Sound(12000, "F3"));
    sounds.add(new Sound(12500, "F3"));
    sounds.add(new Sound(13000, "F3"));
    sounds.add(new Sound(13500, "F3"));
    sounds.add(new Sound(14000, "E3"));
    sounds.add(new Sound(15000, "E3"));

    sounds.add(new Sound(16000, "G3"));
    sounds.add(new Sound(16500, "G3"));
    sounds.add(new Sound(17000, "G3"));
    sounds.add(new Sound(17500, "G3"));
    sounds.add(new Sound(18000, "C3"));
  }

  void loadLeer() {

    songLength = 16000; //in milliseconds
    firstOct = 3;

    bgC = color (0, 0, 40);
    lineC = color (0, 50, 0);
    noteC = color (255, 0, 0);
    timeC = color (0, 0, 255);
    selectC = color (255, 255, 255);

    sounds = new ArrayList<Sound>();

    //sounds.add(new Sound(0, "C3"));
  }
}
