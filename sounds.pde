void musicbox() {
  // mi is not existing, oh why?
  String f = "514__eliasheuninck__musicbox/";
  String e = ".aiff";
  myInstStrings[0] = new InstString("C3", true);
  mySoundFiles[0] = new SoundFile(this, f + "9269__eliasheuninck__do-1" + e);
  myInstStrings[1] = new InstString("D3", true);
  mySoundFiles[1] = new SoundFile(this, f + "9278__eliasheuninck__re-1" + e);
  myInstStrings[2] = new InstString("E3", true);
  mySoundFiles[2] = new SoundFile(this, f + "9277__eliasheuninck__mi-re" + e);
  myInstStrings[3] = new InstString("F3", true);
  mySoundFiles[3] = new SoundFile(this, f + "9272__eliasheuninck__fa-kruis-1" + e);
  myInstStrings[4] = new InstString("G3", true);
  mySoundFiles[4] = new SoundFile(this, f + "9282__eliasheuninck__sol-1" + e);
  myInstStrings[5] = new InstString("A3", true);
  mySoundFiles[5] = new SoundFile(this, f + "9275__eliasheuninck__la-1" + e);
  myInstStrings[6] = new InstString("B3", true);
  mySoundFiles[6] = new SoundFile(this, f + "9267__eliasheuninck__ci-5" + e);
  myInstStrings[7] = new InstString("C4", true);
  mySoundFiles[7] = new SoundFile(this, f + "9270__eliasheuninck__do-chord" + e);
}

void ariokaKalimba() {
  String f = "3759__arioke__kalimba-samples/tip/";
  String e = ".wav";
  myInstStrings[0] = new InstString("C3", true);
  mySoundFiles[0] = new SoundFile(this, f + "58702__arioke__kalimba-lam04-d3-tip-med" + e);
  myInstStrings[1] = new InstString("D3", true);
  mySoundFiles[1] = new SoundFile(this, f + "58704__arioke__kalimba-lam05-e3-tip-med" + e);
  myInstStrings[2] = new InstString("E3", true);
  mySoundFiles[2] = new SoundFile(this, f + "58706__arioke__kalimba-lam06-f3-tip-med" + e);
  myInstStrings[3] = new InstString("F3", true);
  mySoundFiles[3] = new SoundFile(this, f + "58708__arioke__kalimba-lam07-g3-tip-med" + e);
  myInstStrings[4] = new InstString("G3", true);
  mySoundFiles[4] = new SoundFile(this, f + "58710__arioke__kalimba-lam08-a3-tip-med" + e);
  myInstStrings[5] = new InstString("A3", true);
  mySoundFiles[5] = new SoundFile(this, f + "58712__arioke__kalimba-lam09-bb3-tip-med" + e);
  myInstStrings[6] = new InstString("B3", true);
  mySoundFiles[6] = new SoundFile(this, f + "58714__arioke__kalimba-lam10-c4-tip-med" + e);
  myInstStrings[7] = new InstString("C4", true);
  mySoundFiles[7] = new SoundFile(this, f + "58716__arioke__kalimba-lam11-d4-tip-med" + e);
}

void nylonStrings() {
  String f = "7398__kyster__notes-on-nylon-strings/";
  String e = ".wav";
  myInstStrings[0] = new InstString("C3", true);
  mySoundFiles[0] = new SoundFile(this, f + "117710__kyster__c" + e);
  myInstStrings[1] = new InstString("D3", true);
  mySoundFiles[1] = new SoundFile(this, f + "117712__kyster__d" + e);
  myInstStrings[2] = new InstString("E3", true);
  mySoundFiles[2] = new SoundFile(this, f + "117714__kyster__e" + e);
  myInstStrings[3] = new InstString("F3", true);
  mySoundFiles[3] = new SoundFile(this, f + "117715__kyster__f" + e);
  myInstStrings[4] = new InstString("G3", true);
  mySoundFiles[4] = new SoundFile(this, f + "117717__kyster__g" + e);
  myInstStrings[5] = new InstString("A3", true);
  mySoundFiles[5] = new SoundFile(this, f + "117682__kyster__1-oct-a" + e);
  myInstStrings[6] = new InstString("B3", true);
  mySoundFiles[6] = new SoundFile(this, f + "117684__kyster__1-oct-b" + e);
  myInstStrings[7] = new InstString("C4", true);
  mySoundFiles[7] = new SoundFile(this, f + "117685__kyster__1-oct-c" + e);
}

void musicboxUbikphonik() {
  String f = "11206__ubikphonik__music-box-samples/";
  String e = ".aiff";
  myInstStrings[0] = new InstString("C3", true);
  mySoundFiles[0] = new SoundFile(this, f + "177948__ubikphonik__c1" + e);
  myInstStrings[1] = new InstString("D3", true);
  mySoundFiles[1] = new SoundFile(this, f + "177950__ubikphonik__db1" + e);
  myInstStrings[2] = new InstString("E3", true);
  mySoundFiles[2] = new SoundFile(this, f + "177954__ubikphonik__eb1" + e);
  myInstStrings[3] = new InstString("F3", true);
  mySoundFiles[3] = new SoundFile(this, f + "177943__ubikphonik__f1" + e);
  myInstStrings[4] = new InstString("G3", true);
  mySoundFiles[4] = new SoundFile(this, f + "177941__ubikphonik__g1" + e);
  myInstStrings[5] = new InstString("A3", true);
  mySoundFiles[5] = new SoundFile(this, f + "177947__ubikphonik__ab1" + e);
  myInstStrings[6] = new InstString("B3", true);
  mySoundFiles[6] = new SoundFile(this, f + "177949__ubikphonik__bb2" + e);
  myInstStrings[7] = new InstString("C4", true);
  mySoundFiles[7] = new SoundFile(this, f + "177951__ubikphonik__c2" + e);
}

void yamahaToyPiano() {
  String f = "modularsamples__yamaha-cs-30l-toy-piano/";
  myInstStrings[0] = new InstString("C3", true);
  mySoundFiles[0] = new SoundFile(this, f + "314929__modularsamples__yamaha-cs-30l-toy-piano-c2-small-plick-36-127.aiff");
  myInstStrings[1] = new InstString("D3", true);
  mySoundFiles[1] = new SoundFile(this, f + "314931__modularsamples__yamaha-cs-30l-toy-piano-d2-small-plick-38-127.aiff");
  myInstStrings[2] = new InstString("E3", true);
  mySoundFiles[2] = new SoundFile(this, f + "314933__modularsamples__yamaha-cs-30l-toy-piano-e2-small-plick-40-127.aiff");
  myInstStrings[3] = new InstString("F3", true);
  mySoundFiles[3] = new SoundFile(this, f + "314935__modularsamples__yamaha-cs-30l-toy-piano-f2-small-plick-42-127.aiff");
  myInstStrings[4] = new InstString("G3", true);
  mySoundFiles[4] = new SoundFile(this, f + "314937__modularsamples__yamaha-cs-30l-toy-piano-g2-small-plick-44-127.aiff");
  myInstStrings[5] = new InstString("A3", true);
  mySoundFiles[5] = new SoundFile(this, f + "314938__modularsamples__yamaha-cs-30l-toy-piano-a2-small-plick-45-127.aiff");
  myInstStrings[6] = new InstString("B3", true);
  mySoundFiles[6] = new SoundFile(this, f + "314940__modularsamples__yamaha-cs-30l-toy-piano-b2-small-plick-47-127.aiff");
  myInstStrings[7] = new InstString("C4", true);
  mySoundFiles[7] = new SoundFile(this, f + "314941__modularsamples__yamaha-cs-30l-toy-piano-c3-small-plick-48-127.aiff");
}

void loadPoissonmort() {
  String f = "poissonmort_toy-piano-samplepack/poissonmort__toy-records-";
  String e = "-01.wav";
  myInstStrings[0] = new InstString("C3", true);
  mySoundFiles[0] = new SoundFile(this, f + "01-c3" + e);
  myInstStrings[1] = new InstString("D3", true);
  mySoundFiles[1] = new SoundFile(this, f + "03-d3" + e);
  myInstStrings[2] = new InstString("E3", true);
  mySoundFiles[2] = new SoundFile(this, f + "06-e3" + e);
  myInstStrings[3] = new InstString("F3", true);
  mySoundFiles[3] = new SoundFile(this, f + "07-f3" + e);
  myInstStrings[4] = new InstString("G3", true);
  mySoundFiles[4] = new SoundFile(this, f + "10-g3" + e);
  myInstStrings[5] = new InstString("A3", true);
  mySoundFiles[5] = new SoundFile(this, f + "13-a3" + e);
  myInstStrings[6] = new InstString("B3", true);
  mySoundFiles[6] = new SoundFile(this, f + "17-b3" + e);
  myInstStrings[7] = new InstString("C4", true);
  mySoundFiles[7] = new SoundFile(this, f + "18-c4" + e);
}

void loadSynthesizedPianoNotes() {
  // somehow (why?) it sound-glitches in pause mode
  String f = "SynthesizedPianoNotes/Piano";
  String e = ".wav";
  myInstStrings[0] = new InstString("C3", true);
  mySoundFiles[0] = new SoundFile(this, f + "11" + e);
  myInstStrings[1] = new InstString("D3", true);
  mySoundFiles[1] = new SoundFile(this, f + "13" + e);
  myInstStrings[2] = new InstString("E3", true);
  mySoundFiles[2] = new SoundFile(this, f + "15" + e);
  myInstStrings[3] = new InstString("F3", true);
  mySoundFiles[3] = new SoundFile(this, f + "16" + e);
  myInstStrings[4] = new InstString("G3", true);
  mySoundFiles[4] = new SoundFile(this, f + "18" + e);
  myInstStrings[5] = new InstString("A3", true);
  mySoundFiles[5] = new SoundFile(this, f + "110" + e);
  myInstStrings[6] = new InstString("B3", true);
  mySoundFiles[6] = new SoundFile(this, f + "112" + e);
  myInstStrings[7] = new InstString("C4", true);
  mySoundFiles[7] = new SoundFile(this, f + "113" + e);
}

void uiowaPiano() {
  String f = "uiowa_piano/Piano";
  String e = ".aiff";
  myInstStrings[0] = new InstString("C3", true);
  mySoundFiles[0] = new SoundFile(this, f + "C3" + e);
  myInstStrings[1] = new InstString("D3", true);
  mySoundFiles[1] = new SoundFile(this, f + "D3" + e);
  myInstStrings[2] = new InstString("E3", true);
  mySoundFiles[2] = new SoundFile(this, f + "E3" + e);
  myInstStrings[3] = new InstString("F3", true);
  mySoundFiles[3] = new SoundFile(this, f + "F3" + e);
  myInstStrings[4] = new InstString("G3", true);
  mySoundFiles[4] = new SoundFile(this, f + "G3" + e);
  myInstStrings[5] = new InstString("A3", true);
  mySoundFiles[5] = new SoundFile(this, f + "A3" + e);
  myInstStrings[6] = new InstString("B3", true);
  mySoundFiles[6] = new SoundFile(this, f + "B3" + e);
  myInstStrings[7] = new InstString("C4", true);
  mySoundFiles[7] = new SoundFile(this, f + "C4" + e);
}
