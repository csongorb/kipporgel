# kipporgel - eine Entstehungsgeschichte

## Journal

### 2021-12-30

Or should the fun be comparable to watching the air bubble climbing up inside one of those waterfilled [Magic Wands / Zauberstäbe](https://www.amazon.de/Kuenen-St%C3%BCck-Zauberstab-Glitter-Farben/dp/B003JRHBBC/), along you trying to help it get faster by turning it?

### 2021-12-29b

Did the initial desktop version (versions 0.1 & 0.2 & until July 2019) worked better?
Somehow using the accelerometer on mobile was just such a logical idea. But maybe also a bad one.
Maybe this is about being active. The accelerometer turns the experience into a passive one. I only need to rotate and everything just happens. Causing a sound is not really an active task anymore.

Yes, I think I will finish the outer structures first (swiping through songs, changing the lenght of a song, etc.), but will revisit the core (anticipation!) for sure later.

This also very much feels like an excercise in minimalism. [Again.](https://bitbucket.org/csongorb/oneminutegame)

### 2021-12-29a

I'm coming back to this project again and again. Why?

To be honest: this is just not really working as intended.
It is working, somehow. You can have fun with it for a few minutes / seconds, but this is in a way bad, it's diverting. It's not really the fun I initially wanted (and it's also a more simple type of fun?).

This in a way was always about trying to recapture the moment of anticipation before a note plays on a Spieluhr / Music Box. This moment when you are winding the crank, slowly, and the teeth is more and more bending until... ding!
It's almost like the rolling-a-dice problem: how to digitally recapture the task and moment of rolling an analogue dice?

- https://en.wikipedia.org/wiki/Music_box
- https://de.wikipedia.org/wiki/Spieldose
- the classic ones I know so well: https://kikkerlandeu.com/collections/music-box
- there are also some analog versions with editable music, obviously
	- https://www.spieluhr.de/Spieluhren/Spieluhren-Spielwerk-Melodie-selber-machen.asp

### 2021-12-25

It feels so strange that I'm making something so much related with music-making. If there is one thing I have learned about myself in my life is: I don't have any musicality at all. And I have really tried.

I remember when I (during the first version of the project) was trying to understand the frequencies of musical notes. And now I'm searching for the right sounds for the notes. Which instrument to use? An original Music Box? Or a [Kalimba](https://www.amazon.de/Daumenklavier-hochwertiges-Professiona-Tuninghammer-Musikliebhaber/dp/B07PLSFF7G/)? And: how to get the sounds without having to record them by myself? How many octaves?

### 2019-07-11

Inspired by a conversation (and the work of a student in one of my Game-A-Week classes) trying to revisit (and maybe reanimate) this old project. I started it in the garden by experimenting through a summer break, with some input from my children (they found it funny).

As a first step I will add everything to git and step by step turn it into an Android project, seems to be logical.

## Related Work

- https://www.gamesasresearch.com/mdma
- Digital (and Editable) Music Boxes
	- https://musicboxmaniacs.com/create/
	- http://www.jellybiscuits.com/?page_id=705
