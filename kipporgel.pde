import processing.sound.*;

// import android sensor libraries
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

// import android output libraries
import android.os.Vibrator;

// import some android libraries to prevent / handle sleeping
import android.app.Activity;
import android.view.WindowManager;

Activity act;

int nrStrings = 8;
InstString[] myInstStrings = new InstString[nrStrings];
SoundFile[] mySoundFiles = new SoundFile[nrStrings];

boolean stringPlucked;
int stringPluckedAt;

float speed;
int maxSpeed = 4; // 2 is double-speed, etc.
int loopPos;

float speedChangeSpeed = 0.2;

int actTime;
int lastStep;
int lastTime;
float deltaTime;

int actLoop = 1;

Song[] mySongs;
int songPlaying = 0;

int drawXPos, drawYPos;
int drawWidth, drawHeight;
boolean orientationIsHorizontal;

boolean isTouch;
boolean isLRswipe;
boolean isSnapBack;
boolean isSnapForward;
float xOffset;
PVector touchStartPos = new PVector (0.0, 0.0);
PVector touchVector = new PVector (0.0, 0.0);

// some android sensor variables
Context context;
SensorManager manager;
Sensor sensor;
AccelerometerListener listener;
float ax, ay, az;

Vibrator v;

PFont font;

boolean isPlaying = true;

void setup() {
  fullScreen();
  orientation(PORTRAIT);
  font = loadFont("Tahoma-12.vlw");
  textFont(font, 12 * displayDensity);

  drawXPos = 0;
  drawYPos = 0;
  drawWidth = width;
  drawHeight = height;
  orientationIsHorizontal = false;

  //musicbox();
  ariokaKalimba();
  //nylonStrings();
  //musicboxUbikphonik();
  //yamahaToyPiano();
  //loadPoissonmort();
  //loadSynthesizedPianoNotes();
  //uiowaPiano();

  mySongs = new Song[3];
  mySongs[0] = new Song();
  mySongs[0].loadLeer();
  mySongs[1] = new Song();
  mySongs[1].loadBociBoci();
  mySongs[2] = new Song();
  mySongs[2].loadAlleMeineEntchen();

  // set up android sensor variables
  context = getActivity();
  manager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
  sensor = manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
  listener = new AccelerometerListener();
  manager.registerListener(listener, sensor, SensorManager.SENSOR_DELAY_GAME);

  // set up android output variables
  v = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
}

void draw() {

  background(0);
  updateTime();

  if (isPlaying) {

    if (orientationIsHorizontal) {
      speed = lerp(0, maxSpeed, (-ax / 9.81));
    } else {
      speed = lerp(0, maxSpeed, (ay / 9.81));
    }
    if (speed <= 0) {
      speed = 0;
    }

    updateLoop();
    mySongs[songPlaying].update();
    mySongs[songPlaying].play();

    mySongs[songPlaying].drawEverything(drawXPos, drawYPos, drawWidth, drawHeight);

    fill(255, 255, 255);
    //println(mouseOverNote());
    //println("X: " + ax + "\nY: " + ay + "\nZ: " + az, 0, 0, width, height);
  }

  if (isLRswipe || isSnapBack || isSnapForward) {

    int snapBackSpeed = 2500; // how long it would take for the complete width (in milliseconds)?

    if (isLRswipe) {
      xOffset = touchVector.x;
    }

    if (isSnapBack) {
    }

    if (isSnapForward) {
      if (touchVector.x >= 0) {
        xOffset = (width - touchVector.x) * (-1);
      }
      if (touchVector.x < 0) {
        xOffset = touchVector.x + width;
      }
      // after settin up everything:
      // changing back to normal snap-behaviour
      isSnapBack = true;
      isSnapForward = false;
    }

    float xSnapDistance = 0;
    if (!isLRswipe) {
      xSnapDistance = (width * deltaTime / snapBackSpeed);
    }
    //println ("deltaTime = " + deltaTime);
    //println ("xSnapDistance = " + xSnapDistance);

    if (xOffset >= 0) {
      xOffset = xOffset - xSnapDistance;
      if (xOffset <= 20) {
        isSnapBack = false;
        isSnapForward = false;
      }
    } else {
      xOffset = xOffset + xSnapDistance;
      if (xOffset >= 20) {
        isSnapBack = false;
        isSnapForward = false;
      }
    }

    int songToDrawToo;
    int xDraw = int(drawXPos + xOffset);

    mySongs[songPlaying].drawEverything(xDraw, drawYPos, drawWidth, drawHeight);

    if (xOffset < 0) {
      xDraw = int(drawXPos + xOffset + width);
      songToDrawToo = songPlaying + 1;
      if (songToDrawToo >= 2) {
        songToDrawToo = 2;
      }
    } else {
      xDraw = int(drawXPos + xOffset - width);
      songToDrawToo = songPlaying - 1;
      if (songToDrawToo <= 0) {
        songToDrawToo = 0;
      }
    }
    mySongs[songToDrawToo].drawEverything(xDraw, drawYPos, drawWidth, drawHeight);
  }
}

void updateTime() {
  actTime = millis();
  deltaTime = actTime - lastTime;
  lastStep = int (deltaTime * speed);
  lastTime = actTime;
}

void updateLoop() {
  loopPos = loopPos + lastStep;
  if (loopPos >= mySongs[songPlaying].songLength) {
    loopPos = loopPos - mySongs[songPlaying].songLength;
    actLoop = actLoop + 1;
  }
}

void playString(String note) {
  for (int i = 0; i < nrStrings; i++) {
    if (myInstStrings[i].note == note) {
      mySoundFiles[i].stop();
      mySoundFiles[i].play();

      stringPlucked = true;
      stringPluckedAt = millis();
      //println(note);
    }
  }
}

int loopPos_To_Px(int loopPos, int pxTotal) {
  float f = (float(loopPos) + (float(mySongs[songPlaying].halfStep / 2))) / float(mySongs[songPlaying].songLength);
  int px = int(pxTotal * f);
  if (px >= pxTotal) {
    px = px - pxTotal;
  }
  return px;
}

int relativePx_To_loopPos(int loopPos, int pxTotal) {
  float f = float(loopPos) / float(pxTotal);
  int rLoopPos = int((mySongs[songPlaying].songLength * f) - (mySongs[songPlaying].halfStep / 2));
  return(rLoopPos);
}

int mouseOverLoopPos() {
  int p;
  if (orientationIsHorizontal) {
    p = relativePx_To_loopPos(mouseX - drawXPos, drawWidth);
  } else {
    p = relativePx_To_loopPos(mouseY - drawYPos, drawHeight);
  }
  return(p);
}

int mouseOverLoopStep() {
  int stepLength = mySongs[songPlaying].halfStep;
  int mouseAtStep = (mouseOverLoopPos() + (stepLength / 2)) / stepLength;
  int mOverLoopStep = mouseAtStep * stepLength;
  if (mOverLoopStep >= mySongs[songPlaying].songLength) {
    mOverLoopStep = mySongs[songPlaying].songLength - stepLength;
  }
  return(mOverLoopStep);
}

int mouseOverNoteNr() {
  int noteNr;
  if (orientationIsHorizontal) {
    noteNr = int((nrStrings) - (((mouseY - drawYPos) - (mySongs[songPlaying].noteHeightInPix / 2)) / mySongs[songPlaying].noteHeightInPix));
  } else {
    noteNr = int((0) + (((mouseX - drawXPos) - (mySongs[songPlaying].noteWidthInPix / 2)) / mySongs[songPlaying].noteWidthInPix));
  }
  if (noteNr >= nrStrings) {
    noteNr = nrStrings - 1;
  }
  //println(noteNr);
  return(noteNr);
}

class AccelerometerListener implements SensorEventListener {
  public void onSensorChanged(SensorEvent event) {
    ax = event.values[0];
    ay = event.values[1];
    az = event.values[2];
  }
  public void onAccuracyChanged(Sensor sensor, int accuracy) {
  }
}

public void onStart() {
  super.onStart();

  // prevent sleeping
  act = this.getActivity();
  act.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
}

public void onPause() {
  super.onPause();

  isPlaying = false;
  //println("isPlaying = " + isPlaying);

  // stop all sounds
  for (int i = 0; i < nrStrings; i++) {
    mySoundFiles[i].stop();
  }

  // disable sensor
  if (manager != null) {
    manager.unregisterListener(listener);
  }
}

public void onResume() {
  super.onResume();

  isPlaying = true;
  //println("isPlaying = " + isPlaying);

  // don't consider the elapsed time
  lastTime = millis();

  // restart sensor
  if (manager != null) {
    manager.registerListener(listener, sensor, SensorManager.SENSOR_DELAY_GAME);
  }
}
